<?php
/**
 * Grandi_Liutai_Italiani functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Grandi_Liutai_Italiani
 */

if ( ! function_exists( 'grandi_liutai_italiani_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function grandi_liutai_italiani_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Grandi_Liutai_Italiani, use a find and replace
		 * to change 'grandi_liutai_italiani' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'grandi_liutai_italiani', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Categorie', 'grandi_liutai_italiani' ),
			'menu-2' => esc_html__( 'Pagine', 'grandi_liutai_italiani' ),
			'menu-3' => esc_html__( 'Account', 'grandi_liutai_italiani' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'grandi_liutai_italiani_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		add_theme_support( 'woocommerce' );

	}
endif;
add_action( 'after_setup_theme', 'grandi_liutai_italiani_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function grandi_liutai_italiani_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'grandi_liutai_italiani_content_width', 640 );
}
add_action( 'after_setup_theme', 'grandi_liutai_italiani_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function grandi_liutai_italiani_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'grandi_liutai_italiani' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'grandi_liutai_italiani' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'grandi_liutai_italiani_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function grandi_liutai_italiani_scripts() {
	wp_enqueue_style( 'grandi_liutai_italiani-style', get_stylesheet_uri() );

	wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,700|Roboto+Condensed:400,700|Roboto:400,700', false );

	wp_enqueue_style( 'fontsAwesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', false );
//test ajax
	 wp_enqueue_script( 'ajax-pagination', get_template_directory_uri() . '/js/ajax-pagination.js', array( 'jquery' ), '1.0', true );
	 wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
	 	'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'query_vars' => json_encode( $wp_query->query )
	 ));
	 //vai alla riga X per vedere l'hook

	/****************************
	 * FancyBox
	 ****************************/
	wp_enqueue_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js', 'jquery' , true );
	wp_enqueue_style( 'fancybox_style', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css', false );




	/****************************
	 * Instagram e Owlcarousel
	 ****************************/

	wp_enqueue_script( 'instafeed', get_template_directory_uri() . '/js/instafeed.min.js', 'jquery' , true );
	wp_enqueue_style( 'owl1', get_template_directory_uri() . '/js/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css', false );
	wp_enqueue_style( 'owl2', get_template_directory_uri() . '/js/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css', false );
	wp_enqueue_script( 'owl3', get_template_directory_uri() . '/js/OwlCarousel2-2.3.4/dist/owl.carousel.min.js', 'jquery' , true );

	wp_enqueue_script( 'car3', get_template_directory_uri() . '/js/car3.js', 'jquery' , true );

	/****************************
	 * GSAP
	 ****************************/
	 wp_enqueue_script( 'gsap', get_template_directory_uri() . '/js/greensock-js/src/minified/TweenMax.min.js' , true );
	wp_enqueue_script( 'scroll', get_template_directory_uri() . '/js/greensock-js/src/minified/plugins/ScrollToPlugin.min.js' , true );
	wp_enqueue_script('gsap-script', get_template_directory_uri() . '/js/gsap-main.js', array("gsap","scroll"), '1.0.0' , true );
	// wp_enqueue_script( 'gsap-lite', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenLite.min.js' , true );
	// wp_enqueue_script( 'gsap-max', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js' , true );




	wp_enqueue_script( 'grandi_liutai_italiani-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'menu', get_template_directory_uri() . '/js/menu.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'grandi_liutai_italiani-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'grandi_liutai_italiani-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'grandi_liutai_italiani_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


















/**
 * Custom Action Hooks.
 */

remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );



//sposto il bottone carrello nel div che si attiva in hover
remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10);
add_action('aggiungiAlCarrello','woocommerce_template_loop_add_to_cart', 14);





//sposto il bottone quickview nel div che si attiva in hover
remove_action('woocommerce_before_shop_loop_item_title','xoo_qv_button',11);
add_action('quickview_button','xoo_qv_button',10);
//sposto il bottone wishlist nel div che si attiva in hover
remove_action('woocommerce_after_shop_loop_item','tinvwl_view_addto_htmlloop', 10);
add_action('wishlist_button','tinvwl_view_addto_htmlloop', 10);




//sposto il titolo prodotto nel div che si attiva in hover
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
add_action('quickView','woocommerce_template_loop_product_title',10);

//sposto il prezzo prodotto nel div che si attiva in hover
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action('quickView','woocommerce_template_loop_price',11);



//aggiungo "you had your eyes on"
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
// add_action('woocommerce_after_shop_loop', 'wrvp_recently_viewed_products', 10 );

function wrvp_recently_viewed_products(){
	// echo do_shortcode('[wrvp_recently_viewed_products]');
	// echo do_shortcode('[yith_similar_products title="Potresti essere interessato a:" similar_type="cats" order="rand" cat_most_viewed="no" slider="no" prod_type="viewed" num_post="4" num_columns="4"]');


}//FINE AVEVI GLI OCCHI SU

// rimuovo flash sale da content-product.php
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

//rimuovo shop in home
add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );


//Single product
//sposto titolo a dx nel summary
remove_action('woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash', 10);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_title', 5);
add_action('woocommerce_single_product_summary','woocommerce_template_single_title', 9);
remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs', 10);
// add_action('woocommerce_single_product_summary','woocommerce_output_product_data_tabs', 35);
// add_action('woocommerce_single_product_summary','woocommerce_product_options_sku', 45);

//sposto la tabella delle variazioni
// remove_action('Ni_Product_Variations_Table','ni_remove_variable_product_add_to_cart',10);
// remove_action('Ni_Product_Variations_Table','ni_remove_variable_product_add_to_cart',10);
// remove_action('woocommerce_after_single_product_summary','ni_woocommerce_after_single_product_summary', 10);
// remove_action('woocommerce_single_product_summary' , 'woocommerce_template_single_meta' , 40);

// SE IL PRODOTTO HA LE VARIANTI CON LO STESSO PREZZO ALLORA FAI SPARIRE IL PRIZZO IN GRASSETTO
// 1.TROVARE I TEMPLATE TAGS DI WC CHE CONTENGONO IL VALORE DEL PREZZO DELLE VARIABILI
// 2.FARE UN IF CHE SE TUTTE LE FARIABILI HANNO LO STESSO PREZZO ALLORA RIMUOVI L'HOOK
// 3. ALTERNATIVA SE IL PRODOTTO HA LE VARIABILI ALLORA RIMUOVI L-HOOK PREZZO
// remove_action('woocommerce_single_product_summary' , 'woocommerce_template_single_price' , 10);


// INSTRAGRAM
// quello che ci sta dentro si trova nella cartella js/car3.js; usa la combinazione di instafeed con owlcarousel 2. Tutto si trova nella cartella js

add_action('instagram','instragramFeed', 10);


function instragramFeed(){
	echo '<div id="instagram" class="owl-carousel">
	</div>';
};



// disabilito tutto il css wc
// add_filter( 'woocommerce_enqueue_styles', '__return_false' );


/**
 * woo_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0
 * @return      void
*/
function woo_hide_page_title() {

	return false;
}

//questa funzione crea l'HTML che circonda le immagini con l'ffetto lightbox dato da data-fancybox
function carouselPersonalizzato( $attachment_id, $main_image ) {

		$flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
		// $gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
		$carosello = array( 400 , 400);
		$thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( $gallery_thumbnail['width'], $gallery_thumbnail['height'] ) );
		$image_size        = apply_filters( 'woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_single' : $thumbnail_size );
		$full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
		$thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
		$full_src          = wp_get_attachment_image_src( $attachment_id, $full_size );
		// $image             = wp_get_attachment_image( $attachment_id, $image_size, false, array(
		$image             = wp_get_attachment_image( $attachment_id, $full_size, false, array(
		'title'                   => get_post_field( 'post_title', $attachment_id ),
		'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
		'data-src'                => $full_src[0],
		'data-large_image'        => $full_src[0],
		'data-large_image_width'  => $full_src[1],
		'data-large_image_height' => $full_src[2],
		'class'                   => $main_image ? 'wp-post-image' : '',
		)
	);
	$image2             = wp_get_attachment_image( $attachment_id, $carosello, false, array(
	'title'                   => get_post_field( 'post_title', $attachment_id ),
	'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
	'data-src'                => $full_src[0],
	'data-large_image'        => $full_src[0],
	'data-large_image_width'  => $full_src[1],
	'data-large_image_height' => $full_src[2],
	'class'                   => $main_image ? 'wp-post-image' : '',
	)
);

	if ( $main_image == true) {
		# code...
		return '<a class="item" href="' . esc_url( $full_src[0] ) . '" data-fancybox="principale">'. $image . '</a>' ;
	}else{
		return '<a class="item" href="' . esc_url( $full_src[0] ) . '" data-fancybox="gallery">'. $image2 . '</a>' ;
	}
	/*
	l'originale faceva cosi':
	return '<div data-thumb="' . esc_url( $thumbnail_src[0] ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_src[0] ) . '">' . $image . '</a></div>';
	*/
};


/*
wp_ajax_[action_name] format are only executed for logged in users.
Hooks that take on the wp_ajax_norpiv_[action_name] format are only executed for non-logged in users.
The great benefit of this is that you can very easily separate functionality.
The action names I mentioned above refer to the action defined in our
AJAX call in Javascript (action: 'ajax_pagination') – they must match.
*/
//
// add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
// add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );
//
// function my_ajax_pagination() {
//     echo do_shortcode('[product_categories number="0" parent="0"]');
//     die();
// }//fine funzione
//
// function my_image_size_override() {
//     return array( 825, 510 );
// }



// //questo serve a cambiare il loop di WC
//
// add_action( 'woocommerce_product_query', 'so_20990199_product_query' );
//
// function so_20990199_product_query( $q ){
//
//     $product_ids_on_sale = wc_get_product_ids_on_sale();
//
// 		// $q->set( 'post__in', (array) $product_ids_on_sale );
//     $q->set( 'post_parent', 0 );
//
// }
/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 100;
  return $cols;
}


//TABELLE AL POSTO DEL DEL MENU A TENDINA





//AGGIUNGO SPUNTA PER CONTROLLO PARTITA IVA
add_action('woocommerce_after_checkout_billing_form', 'cw_custom_checkbox_fields');


function cw_custom_checkbox_fields( $checkout ) {

    // echo '<div class=""><p>'.__('VAT Number Policy: We will check your VAT Number before shipping process starts.').'</p>';
		//
    // woocommerce_form_field( 'custom_checkbox', array(
    //     'type'          => 'checkbox',
    //     'label'         => __('I Agree.'),
    //     'required'  => true,
    // ), $checkout->get_value( 'custom_checkbox' ));
		//
    // echo '</div>';

		echo '<div class="politica_iva"><p>'.__('VAT Number Policy: We will check your VAT Number before shipping process starts.').'</p>';
		echo '</div>';
}



/**
 * Rename "home" in breadcrumb
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_home_text' );
function wcc_change_breadcrumb_home_text( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Apartment'
	$defaults['home'] = '&nbsp;';
	return $defaults;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_home_text', 20 );



/**
 * Change the breadcrumb separator
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
function wcc_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = ' > ';
	return $defaults;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter', 20 );

// COME FARE A FARE IL DISCOSO DELL VAT CHE SE E' EXTRA ITALIA ALLORA NON DEVE PAFARE L'IVA.


/**
 * Remove related products output
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );



// CUSTOM Login
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/Logo_Grandi_Liutai_Italiani_sm.png);
		width:100%;
		background-size: contain;
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );




function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


//escludo le class_parents
function exclude_product_cat_children($wp_query) {
if ( isset ( $wp_query->query_vars['product_cat'] ) && $wp_query->is_main_query()) {
$wp_query->set('tax_query', array(
                                array (
                                    'taxonomy' => 'product_cat',
                                    'field' => 'slug',
                                    'terms' => $wp_query->query_vars['product_cat'],
                                    'include_children' => false
                                )
                             )
);
}
}
add_filter('pre_get_posts', 'exclude_product_cat_children');

//funzione per azzera iva se partita iva diversa che da europa
add_action( 'woocommerce_checkout_update_order_review', 'gli_taxexempt_checkout_based_on_vat' );

function gli_taxexempt_checkout_based_on_vat( $post_data ) {
        global $woocommerce;
        $woocommerce->customer->set_is_vat_exempt( true );
        parse_str($post_data);
				$vat_number = strtoupper($vat_number);
				// if ( $vat_number == '32444' ) $woocommerce->customer->set_is_vat_exempt( true);
				if ( substr( $vat_number, 0, 2 ) === "IT"  || empty($vat_number) || is_numeric($vat_number)) $woocommerce->customer->set_is_vat_exempt( false );
        // if ( $vat_number != "IT") $woocommerce->customer->set_is_vat_exempt( true );

};




//INIZIO VAT FIELD

add_filter( 'woocommerce_billing_fields', 'custom_woocommerce_billing_fields' );




function custom_woocommerce_billing_fields($fields)
{

    $fields['vat_number'] = array(
        'label' => __('VAT Number', 'woocommerce'), // Add custom field label
        'placeholder' => _x('Your VAT Number here... (e.g.: IT00000000001)', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        // 'class' => array('my-css'),    // add class name
				'class' =>
				array (
					0 => 'form-row-wide',
					1 => 'update_totals_on_change',
				// 	// 2 => 'update_totals_on_change',
			),
    		);

    return $fields;
}


add_action( 'woocommerce_checkout_update_order_meta', 'wpdesk_checkout_vat_number_update_order_meta' );
/**
* Save VAT Number in the order meta
*/
function wpdesk_checkout_vat_number_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['vat_number'] ) ) {
        update_post_meta( $order_id, '_vat_number', sanitize_text_field( $_POST['vat_number'] ) );
    }
}




add_action( 'woocommerce_admin_order_data_after_billing_address', 'wpdesk_vat_number_display_admin_order_meta', 10, 1 );
/**
 * Display VAT Number in order edit screen
 */
function wpdesk_vat_number_display_admin_order_meta( $order ) {
    echo '<p><strong>' . __( 'VAT Number', 'woocommerce' ) . ':</strong> ' . get_post_meta( $order->id, '_vat_number', true ) . '</p>';
}

add_filter( 'woocommerce_email_order_meta_keys', 'wpdesk_vat_number_display_email' );
/**
* VAT Number in emails
*/
function wpdesk_vat_number_display_email( $keys ) {
     $keys['VAT Number'] = '_vat_number';
     return $keys;
};

/**
 * @snippet       Removes shipping method labels @ WooCommerce Cart
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=484
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.2
 */

add_filter( 'woocommerce_cart_shipping_method_full_label', 'gli_remove_shipping_label', 10, 2 );

function gli_remove_shipping_label($label, $method) {
$new_label = preg_replace( '/^.+:/', '', $label );
return $new_label;
}

// function XXX( $fields = array() ){
// 	echo "<pre>";
// 	var_export( $fields );
// 	echo "</pre>";
//
// 	return $fields;
// }
// add_filter('woocommerce_billing_fields', 'XXX');


add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a rel="nofollow" class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><i class="fas fa-shopping-cart"></i> (<?php echo sprintf ( WC()->cart->get_cart_contents_count() , WC()->cart->get_cart_contents_count() ); ?>)</a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

