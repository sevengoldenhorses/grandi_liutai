<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grandi_Liutai_Italiani
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="banner-info">

			<div class="banner_blocco ">
				<img src="<?php echo get_template_directory_uri(). '/img/icona1.png';?>  " alt="" style="width: 16%;">
				<p>Consegna in 2-3 giorni lavorativi</p>
			</div>

			<div class="banner_blocco ">
				<img src="<?php echo get_template_directory_uri(). '/img/icona2.png';?>  " alt="" style="width: 15%;">
				<p>Scrivici a info@grandiliutai.it</p>

			</div>

			<div class="banner_blocco ">
				<!-- <i class="fas fa-headset"></i> -->
				<img src="<?php echo get_template_directory_uri(). '/img/icona3.png';?>  " alt="" style="width: 12%;">
				<p>Restituisci il tuo articolo gratuitamente!</p>

			</div>

			<div class="banner_blocco ">
				<img src="<?php echo get_template_directory_uri(). '/img/icona4.png';?>  " alt="" style="width: 12%;">
				<p> Iscriviti alla nosrta newsletter gratuitamente</p>

			</div>


		</div>
		<div class="carteDiCredito">
			<img src="<?php echo get_template_directory_uri() . '/img/footer_cartedicredito3.jpg'; ?>" />
		</div>
		<div class="site-info">
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( '© Copyright 2018 %1$s %2$s.', 'grandi_liutai_italiani' ), '<strong>Grandi Liutai Italiani</strong> di Giovanni Colonna ', '<a href="http://sevengoldenhorses.com">All right reserved</a>');
				?>
				<p>Corso Matteotti, 11 - 26100 Cremona  - ITALY</p>
				<p>C.F. CLNGNN78M24C3888J</p>
				<p>P.IVA 01268890199 - REA CR0163356</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
