<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grandi_Liutai_Italiani
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <a href="#primary-menu" class="menu-toggle" role="button" id="menu-principale-toggle" aria-expanded="false" aria-controls="main-menu" aria-label="Open main menu">MENU</a>

	<a id=ascensore><i class="fa fa-angle-up"></i></a>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'grandi_liutai_italiani' ); ?></a>

	<header id="masthead" class="site-header">
<!-- inizio social -->
	<div class="contattiHeader">
		<div class="contDX">
			<a href="https://www.facebook.com/grandiliutaiitaliani/"><i class="fab fa-facebook"></i></a>
			<a href="https://www.instagram.com/grandiliutaiitaliani/"><i class="fab fa-instagram"></i></a>
			<a href="mailto:info@grandiliutai.it"> <i class="fas fa-envelope"></i> </a>

		</div>
		<div class="contSX">
			<!-- <i class="fas fa-share"></i> -->
			<!-- <i class="fas fa-share-square"></i> -->

			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"> <i class="fas fa-user"></i></a>
			<!-- <a href="<?php echo wc_get_cart_url(); ?>"><i class="fas fa-shopping-cart"></i></a> -->
			<!-- <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?><?php echo WC()->cart->get_cart_total(); ?></a> -->
			<div class="carrello">
				<a rel="nofollow" class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><i class="fas fa-shopping-cart"></i> (<?php echo sprintf ( WC()->cart->get_cart_contents_count() , WC()->cart->get_cart_contents_count() ); ?>)</a>
			</div>
		</div>
	</div>


		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">




				<img src="<?php echo get_template_directory_uri() . '/img/Logo_Grandi_Liutai_Italiani_sm.png' ?>;"/>


			</a>


				</a></p>

				<?php
			endif;
			$grandi_liutai_italiani_description = get_bloginfo( 'description', 'display' );
			if ( $grandi_liutai_italiani_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $grandi_liutai_italiani_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Categorie', 'grandi_liutai_italiani' ); ?></button> -->
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'menu-categorie',
			) );
			?>

			<!-- search -->
			<?php get_search_form() ?>


			<!-- secondo menu -->

			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'menu-pagine',
			) );
			?>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-3',
				'menu_id'        => 'menu-account',
			) );
			?>
		</nav><!-- #site-navigation -->
		<div id="retro">

		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
