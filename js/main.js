/*--------------------------------------------------------------
## MENU
--------------------------------------------------------------*/

//1. selezionare l'elemento bottone menu. Quando cliccato sparisce verso SX
var $bottone = document.getElementById('menu-principale-toggle');
var $menu = document.getElementById('site-navigation');
var $sfondo = document.getElementById('retro');

$bottone.onclick = function() {
    TweenMax.to($bottone, 0.5, {
        x: -15
    });
    TweenMax.to($sfondo, 0.3, {css:{opacity:"0.2", visibility:"visible"},ease:Power2.easeIn});
    TweenMax.to($menu, 0.8, {
        x: 270
    });
};

$sfondo.onclick = function() {
    TweenMax.to($bottone, 1, {
        x: 0
    });

    TweenMax.to($menu, 0.8, {
        x: -270
    });
    TweenMax.to($sfondo, 0.5, {css:{opacity:"0", visibility:"hidden"}});
};



//2. prendere elemento menu div e farlo apparire con un effetto di transizione ease
//3. extra aggiungere una classe al body con un nero con opacita' .5
//4. se clicchi fuori dal menu' allora chiudi tutto e fai riapparire il bottone menu

/*--------------------------------------------------------------
## ASCENSORE
--------------------------------------------------------------*/

var $ascensore = document.getElementById('ascensore');

$ascensore.onclick = function() {
    TweenLite.to(window, 0.5, {
        scrollTo: {
            y: "#masthead"
        }
    });
};

(function($) {
    $($ascensore).hide();
    $(window).scroll(function() {

        var scroll = $(window).scrollTop();
        if (scroll >= 900) {
            $($ascensore).show();
        }
        if (scroll < 899) {
            $($ascensore).hide();
        }
    });

})(jQuery);

/*--------------------------------------------------------------
## OWLCAROUSEL
--------------------------------------------------------------*/
(function($) {
    "use strict";
	$(document).ready(function() {
			$(".owl-carousel").owlCarousel(

					{
							autoWidth: true,
							center: true,
							loop: true,
							margin: 10,
							nav: true,
							responsive: {
									0: {
											items: 1
									},
									600: {
											items: 3
									},
									1000: {
											items: 5
									}
							}
					}
			);
	});

})(jQuery);
