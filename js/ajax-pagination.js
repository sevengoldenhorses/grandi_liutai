(function($) {


	$(document).on( 'click', '.bottoneAjax', function( event ) {
    $('.products.columns-4').fadeOut(500);


		$.ajax({
			url: ajaxpagination.ajaxurl,
			type: 'post',
			data: {
				action: 'ajax_pagination',
			},
			success: function( response ) {    //o lasciare response
        $(document).find('.products.columns-4').html(response).fadeIn(500);

			}
		})//fine ajax
	})
})(jQuery);


/**
If we don’t supply an action the admin-ajax.php script dies and returns 0.
 If we do supply an action but we don’t hook into the required WordPress
 hooks nothing happens and at the very end of the file we die again,
 returning 0.

 To get a meaningful answer from WordPress we need to define some WordPress
 actions. This is done using a set pattern.


 add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

function my_ajax_pagination() {
    echo get_bloginfo( 'title' );
    die();
}
*/
